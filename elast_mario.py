from firedrake import *
import numpy as np
import MMPDE as ms

from computeMetric import *


def deriv_vectors(f):
  if ndim == 1:
    dxdxi = Dx(f[0], 0)
    dxdet = 0.0
    dxdze = 0.0
    dydxi = 0.0
    dydet = 1.0
    dydze = 0.0
    dzdxi = 0.0
    dzdet = 0.0
    dzdze = 1.0
  elif ndim == 2:
    dxdxi = Dx(f[0], 0)
    dxdet = Dx(f[0], 1)
    dxdze = 0.0
    dydxi = Dx(f[1], 0)
    dydet = Dx(f[1], 1)
    dydze = 0.0
    dzdxi = 0.0
    dzdet = 0.0
    dzdze = 1.0
  elif ndim == 3:
    dxdxi = Dx(f[0], 0)
    dxdet = Dx(f[0], 1)
    dxdze = Dx(f[0], 2)
    dydxi = Dx(f[1], 0)
    dydet = Dx(f[1], 1)
    dydze = Dx(f[1], 2)
    dzdxi = Dx(f[2], 0)
    dzdet = Dx(f[2], 1)
    dzdze = Dx(f[2], 2)

  ap = {}
  ap[0] = as_vector([dxdxi, dydxi, dzdxi])
  ap[1] = as_vector([dxdet, dydet, dzdet])
  ap[2] = as_vector([dxdze, dydze, dzdze])

  Jp = dot(ap[0], cross(ap[1], ap[2]))

  ac = {}
  ac[0] = cross(ap[1], ap[2]) 
  ac[1] = cross(ap[2], ap[0]) 
  ac[2] = cross(ap[0], ap[1]) 

  Jc = dot(ac[0], cross(ac[1], ac[2])) 

  if ndim == 2:
    ad = {}
    ad[0] = as_vector([ac[0][i] for i in range(2)])
    ad[1] = as_vector([ac[1][i] for i in range(2)])
    return ap, ad, Jp, Jc

  return ap, ac, Jp, Jc


# =============================================================================
# Problem options
# =============================================================================


meshfile = 'square.msh'
#U_exp = Expression('1. - 100*A*tanh(10*(x[0]*x[0]+x[1]*x[1])-2.5)', A=0.005)
U_exp = Expression("(0.99*(fabs(x[0]*x[1])<2*pi/50)+0.01)*sin(50*x[0]*x[1])")

maptype = 'mmpde'
formulation = 'linear'
nu = 0.33

cmesh = Mesh(meshfile)
pmesh = Mesh(meshfile)
ndim = cmesh.topological_dimension()
cmesh._plex.view()

# Computational function space and trial/test functions
Wc = VectorFunctionSpace(cmesh, 'CG', 1)
u = TestFunction(Wc)
x = TrialFunction(Wc)

# Initialize solution function
if ndim == 1:
  solution = project(Expression(('x[0]',)), Wc)
elif ndim == 2:
  solution = project(Expression(('x[0]','x[1]')), Wc)
elif ndim == 3:
  solution = project(Expression(('x[0]','x[1]','x[2]')), Wc)

# Initialize boundary conditions
bc_exp = Expression(('x[0]','x[1]'))
bcs = [DirichletBC(Wc, bc_exp, 1), DirichletBC(Wc, bc_exp, 2), DirichletBC(Wc, bc_exp, 3), DirichletBC(Wc, bc_exp, 4)]

# Computational function space for the monitor function
Mspace_c = FunctionSpace(cmesh, 'CG', 1)
Mc = Function(Mspace_c)
Mc = project(Expression('1.0'), Mspace_c)

# build the problem
omega = Mc
a = omega*inner(grad(x), grad(u))*dx
#L = dot(grad(omega), u)*dx
L = -omega*div(u)*dx

problem = LinearVariationalProblem(a, L, solution, bcs=bcs)#bcs.values())
solver = LinearVariationalSolver(problem,  solver_parameters={#'pc_type': 'ilu',
                                                                #'ksp_rtol': 1e-5,
                                                                #'ksp_max_it' : 1000,
                                                                'ksp_converged_reason': True,
                                                                'ksp_monitor_true_residual': False,
                                                                'ksp_view': False}) # can add solver_parameters
solver.parameters['linear_solver'] = 'lu'


for j in range(50):
  Uspace = FunctionSpace(pmesh, 'CG', 1)
  U = project(U_exp, Uspace)
  Mspace = FunctionSpace(pmesh, 'CG', 1)
  M = sqrt(1+Dx(U,0)**2 + Dx(U,1)**2)
  Mp = project(M, Mspace)
  
  Mc.dat.data[:] = Mp.dat.data

  # direct solves
  solver.solve()


  if ndim == 1:
    pmesh.coordinates.dat.data[:,0] += solution.dat.data[:]
  elif ndim == 2:
    pmesh.coordinates.dat.data[:,0] += solution.dat.data[:,0]
    pmesh.coordinates.dat.data[:,1] += solution.dat.data[:,1]
  elif ndim == 3:
    pmesh.coordinates.dat.data[:,0] += solution.dat.data[:,0]
    pmesh.coordinates.dat.data[:,1] += solution.dat.data[:,1]
    pmesh.coordinates.dat.data[:,2] += solution.dat.data[:,2]



Uspace = FunctionSpace(pmesh, 'CG', 1)
U = Function(Uspace).interpolate(U_exp)

entity_dofs = np.zeros(ndim+1, dtype=np.int32)
entity_dofs[0] = pmesh.geometric_dimension()
coordSection = pmesh._plex.createSection([1], entity_dofs, perm=pmesh.topology._plex_renumbering)
    
plex = pmesh._plex
vStart, vEnd = plex.getDepthStratum(0)
nbrVer = vEnd - vStart
    
dmCoords = pmesh.topology._plex.getCoordinateDM()
dmCoords.setDefaultSection(coordSection)    
    
with pmesh.coordinates.dat.vec_ro as coords:
  pmesh.topology._plex.setCoordinatesLocal(coords)
writeGmf(pmesh, 1, "boundary_ids", "pmesh", U, 1, "pmesh", coordSection)