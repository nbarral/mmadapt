from firedrake import *
import numpy as np


#mesh = UnitIntervalMesh(10)
#V = FunctionSpace(mesh, 'CG', 1)
#x = Function(V)
#v = TestFunction(V)
#a = Dx(x)*Dx(v)*dx
#L = 0
#bc_exp = Expression('x[0]')
#bcs = [DirichletBC(V, bc_exp, 1), DirichletBC(V, bc_exp, 2)]
#solve(a==L, x, bcs=bcs)
#print mesh.coordinates.dat.data
#print x.dat.data


meshfile = 'square.msh'
#mesh_p = Mesh(meshfile)
n = 200
mesh_p = UnitSquareMesh(n,n)

#U_exp = Expression('1. - 100*A*tanh(10*((x[0]-c)*(x[0]-c)+x[1]*x[1])-2.5)', A=0.005, c=-0.2+0.4/nt*i)
#U_exp = Expression("1+(0.99*(fabs((x[0]-0.5)*(x[1]-0.5))<2*pi/50)+0.01)*sin(50*(x[0]-0.5)*(x[1]-0.5))")
#U_exp = Expression("((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.5)*(x[1]-0.5)) < 0.3*0.3")
U_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.5)*(x[1]-0.5)-0.2*0.2))", alpha = 5000)

#U_exp = Expression("c*exp(-c*c*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.5)*(x[1]-0.5)))", c=100)


U_p = FunctionSpace(mesh_p, 'CG', 1)
u = Function(U_p).interpolate(U_exp)

ndim = 2
entity_dofs = np.zeros(ndim+1, dtype=np.int32)
entity_dofs[0] = mesh_p.geometric_dimension()
coordSection = mesh_p._plex.createSection([1], entity_dofs, perm=mesh_p.topology._plex_renumbering)

plex = mesh_p._plex
vStart, vEnd = plex.getDepthStratum(0)
nbrVer = vEnd - vStart

dmCoords = mesh_p.topology._plex.getCoordinateDM()
dmCoords.setDefaultSection(coordSection)    

with mesh_p.coordinates.dat.vec_ro as coords:
    mesh_p.topology._plex.setCoordinatesLocal(coords)
writeGmf(mesh_p, 1, "boundary_ids", "pouet_ini", u, 1, "pouet_ini", coordSection)

#option_ts = 'underrelax'
option_ts = 'pseudo_ts'
theta = 0.2
nt = 200
ord = 1
for i in range(nt):

    print("######  Iteration %d" % i)
#    mesh_c = Mesh(meshfile)
    mesh_c = UnitSquareMesh(n,n)
    mesh_p = Mesh(mesh_p.coordinates)
     
    U_p = FunctionSpace(mesh_p, 'CG', ord)
    u_p = Function(U_p).interpolate(U_exp)
#    w_p = Function(U_p).interpolate(sqrt(1+ Dx(u_p,0)*Dx(u_p,0) + Dx(u_p,1)*Dx(u_p,1)))

    U_c = FunctionSpace(mesh_c, 'CG', ord)
    V_c = VectorFunctionSpace(mesh_c, 'CG', ord)

    v = TestFunction(V_c)
    x = Function(V_c)
    x_new = TrialFunction(V_c)
    
    w_c = Function(U_c)
#    w_c.dat.data[:] = w_p.dat.data
    u_c = Function(U_c)
    u_c.dat.data[:] = u_p.dat.data 
    w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1)))

    bc_exp = Expression(('x[0]', 'x[1]'))
    bcs = DirichletBC(V_c, bc_exp, "on_boundary")


    ################################################
    ##########  Smooth monitor function   ##########
    ################################################

    sigma = Constant(0.001)
#    sigma2 = np.max(np.abs(u_c.dat.data)) / np.max(np.abs(w_c.dat.data))
#    sigma2 = 100*0.01*sigma2
#    print "sigma: ", sigma.dat.data[0], " sigma2: ", sigma2
    Mn = TrialFunction(U_c)
    um = TestFunction(U_c)

    bcs_smoothMF = DirichletBC(U_c, w_c, 'on_boundary')

    H = zero()
    H += Mn*um*dx + sigma*dot(grad(Mn), grad(um))*dx - w_c*um*dx

    solve(lhs(H) == rhs(H), w_c, bcs = bcs_smoothMF)
#    self.Mc.assign(M)


    ###################################
    ##########  Solve MMPDE  ##########
    ###################################

    if option_ts == 'underrelax':

        M_c = w_c*as_tensor([[1,0],[0,1]])
        F = inner(M_c, dot(transpose(grad(x)),grad(v)))*dx
#        F =  inner(dot(M_c,grad(x)),grad(v))*dx
        solve(F==0, x, bcs=bcs)
    
        W_c = VectorFunctionSpace(mesh_c, 'CG', 1)
        y = Function(W_c).interpolate(x)
    
        res = np.linalg.norm(abs(mesh_p.coordinates.dat.data-y.dat.data))
        print res
        if res < 0.0005 : break
    
        mesh_p.coordinates.dat.data[:,0] = theta*y.dat.data[:,0] + (1-theta)*mesh_p.coordinates.dat.data[:,0]
        mesh_p.coordinates.dat.data[:,1] = theta*y.dat.data[:,1] + (1-theta)*mesh_p.coordinates.dat.data[:,1]


    elif option_ts == 'pseudo_ts':

        W_c = VectorFunctionSpace(mesh_c, 'CG', 1)
        y = Function(W_c)
        y.dat.data[:] = mesh_p.coordinates.dat.data
        x.interpolate(y)

        M_c = w_c*as_tensor([[1,0],[0,1]])
#        F = (x-x_old)*v*dx# + dt*inner(M_c, dot(transpose(grad(0.5*(x+x_old))),grad(v)))*dx
        dt = Constant(0.01)
        theta_ts = Constant(0.75)
        F = dot(x_new-x,v)*dx + dt*inner(dot(M_c,grad(theta_ts*x_new +(1-theta_ts)*x)),grad(v))*dx
        solve(lhs(F)==rhs(F), x, bcs=bcs)    

        y.interpolate(x)

        res = np.linalg.norm(abs(mesh_p.coordinates.dat.data-y.dat.data))
        print res
        if res < 0.0005 : break

        mesh_p.coordinates.dat.data[:,0] = y.dat.data[:,0]
        mesh_p.coordinates.dat.data[:,1] = y.dat.data[:,1]
    
    File("pouet.pvd").write(mesh_p.coordinates)

    U_p = FunctionSpace(mesh_p, 'CG', 1)
    u = Function(U_p).interpolate(U_exp)

    ndim = 2
    entity_dofs = np.zeros(ndim+1, dtype=np.int32)
    entity_dofs[0] = mesh_p.geometric_dimension()
    coordSection = mesh_p._plex.createSection([1], entity_dofs, perm=mesh_p.topology._plex_renumbering)
    
    plex = mesh_p._plex
    vStart, vEnd = plex.getDepthStratum(0)
    nbrVer = vEnd - vStart
    
    dmCoords = mesh_p.topology._plex.getCoordinateDM()
    dmCoords.setDefaultSection(coordSection)    
    
    with mesh_p.coordinates.dat.vec_ro as coords:
        mesh_p.topology._plex.setCoordinatesLocal(coords)
    writeGmf(mesh_p, 1, "boundary_ids", "pouet.%d" %i, u, 1, "pouet.%d" %i, coordSection)

with mesh_p.coordinates.dat.vec_ro as coords:
    mesh_p.topology._plex.setCoordinatesLocal(coords)
writeGmf(mesh_p, 1, "boundary_ids", "final_mesh", u, 1, "final_mesh", coordSection)    