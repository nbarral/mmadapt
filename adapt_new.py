from firedrake import *
import numpy as np
import MMPDE as ms

from computeMetric import *


def deriv_vectors(f):
  if ndim == 1:
    dxdxi = Dx(f[0], 0)
    dxdet = 0.0
    dxdze = 0.0
    dydxi = 0.0
    dydet = 1.0
    dydze = 0.0
    dzdxi = 0.0
    dzdet = 0.0
    dzdze = 1.0
  elif ndim == 2:
    dxdxi = Dx(f[0], 0)
    dxdet = Dx(f[0], 1)
    dxdze = 0.0
    dydxi = Dx(f[1], 0)
    dydet = Dx(f[1], 1)
    dydze = 0.0
    dzdxi = 0.0
    dzdet = 0.0
    dzdze = 1.0
  elif ndim == 3:
    dxdxi = Dx(f[0], 0)
    dxdet = Dx(f[0], 1)
    dxdze = Dx(f[0], 2)
    dydxi = Dx(f[1], 0)
    dydet = Dx(f[1], 1)
    dydze = Dx(f[1], 2)
    dzdxi = Dx(f[2], 0)
    dzdet = Dx(f[2], 1)
    dzdze = Dx(f[2], 2)

  ap = {}
  ap[0] = as_vector([dxdxi, dydxi, dzdxi])
  ap[1] = as_vector([dxdet, dydet, dzdet])
  ap[2] = as_vector([dxdze, dydze, dzdze])

  Jp = dot(ap[0], cross(ap[1], ap[2]))

  ac = {}
  ac[0] = cross(ap[1], ap[2]) 
  ac[1] = cross(ap[2], ap[0]) 
  ac[2] = cross(ap[0], ap[1]) 

  Jc = dot(ac[0], cross(ac[1], ac[2])) 

  if ndim == 2:
    ad = {}
    ad[0] = as_vector([ac[0][i] for i in range(2)])
    ad[1] = as_vector([ac[1][i] for i in range(2)])
    return ap, ad, Jp, Jc

  return ap, ac, Jp, Jc


# =============================================================================
# Problem options
# =============================================================================

smoothers = ['laplacian', 'winslow', 'linelas']
mmpdes = ['candh','wvd','mmpde']

meshfile = 'square.msh'

U_exp = Expression('1. - 100*A*tanh(10*((x[0]-0.2)*(x[0]-0.2)+x[1]*x[1])-2.5)', A=0.005)
U_exp = Expression("1+(0.99*(fabs(x[0]*x[1])<2*pi/50)+0.01)*sin(50*x[0]*x[1])")
#U_exp = Expression("0.3*((x[0])*(x[0]) + (x[1])*(x[1]) < 0.45*0.45)")
#U_exp = Expression('1.0+100*A*exp(-(pow((x[0]-B),2)+pow((x[1]-C),2))/(2.0*pow(F,2)))+100*A*exp(-(pow((x[0]-D),2)+pow((x[1]-E),2))/(2.0*pow(F,2)))',
#                     A=0.04, B=-0.3, C=-0.5, D=0.3, E=-0.5, F=0.1)

maptype = 'candh'
formulation = 'linear'
nu = 0.33

cmesh = Mesh(meshfile)
pmesh = Mesh(meshfile)
ndim = cmesh.topological_dimension()
cmesh._plex.view()

# Computational function space and trial/test functions
Wc = VectorFunctionSpace(cmesh, 'CG', 1)
u = TestFunction(Wc)
x = TrialFunction(Wc)
  
# Initialize solution function
if ndim == 1:
  solution = project(Expression(('x[0]',)), Wc)
elif ndim == 2:
  solution = project(Expression(('x[0]','x[1]')), Wc)
elif ndim == 3:
  solution = project(Expression(('x[0]','x[1]','x[2]')), Wc)

# Create Function object to store previous step solution
prev = Function(Wc)
prev.assign(solution)

# Initialize boundary conditions
bc_exp = Expression(('x[0]','x[1]'))
bcs = [DirichletBC(Wc, bc_exp, 1), DirichletBC(Wc, bc_exp, 2), DirichletBC(Wc, bc_exp, 3), DirichletBC(Wc, bc_exp, 4)]

# Computational function space for the monitor function
if not maptype in smoothers:
  Mspace_c = TensorFunctionSpace(cmesh, 'CG', 1)
  Mc = Function(Mspace_c)
  if ndim == 1:
    Mc = project(Expression( (('1.0',),) ), Mspace_c)
  elif ndim == 2:
    Mc = project(Expression( (('1.0','0.0'),
                              ('0.0','1.0')) ), Mspace_c)
  elif ndim == 3:
    Mc = project(Expression( (('1.0','0.0','0.0'),
                              ('0.0','1.0','0.0'),
                              ('0.0','0.0','1.0')) ), Mspace_c)
  Mc_prev = project(Mc, Mspace_c)

# initialize time step and time dependence multiplier tau
dt = Constant(1)
tau = Constant(0)

# build the problem
if maptype == 'linelas':
  if formulation == 'nonlinear':
    formulation = 'linear'

  E = 1.0e4
  lam = (E*nu) / ((1+nu)*(1-2*nu))
  mu = E / (2*(1+nu))
  ones = Constant(((1.,1.),(1.,1.)))
  I = Constant(((1.,0.),(0.,1.)))

  # Weak form with all terms integrated by parts
  F = inner( lam * tr((grad(x)-ones+transpose(grad(x)-ones)))/2 \
    * I + 2 * mu * (grad(x)-ones+transpose(grad(x)-ones))/2, grad(u) ) * dx

elif maptype == 'laplacian':
  if formulation == 'nonlinear':
    formulation = 'linear'

  F = inner(grad(x), grad(u)) * dx

elif maptype == 'candh':
  if formulation == 'nonlinear':
     formulation = 'linear'

#  F = inner(Mc, transpose(grad(x))*grad(u))*dx
#  print lhs(F)
#  print rhs(F)
  F = zero()
  for i in range(ndim):
    for j in range(ndim):
      F += Mc[i,j] * dot(Dx(x, j), Dx(u, i)) * dx
  F += Constant(0)*u[0]*dx

elif maptype == 'winslow':

  if formulation == 'linear':
    ap, ac, Jp, Jc = deriv_vectors(prev)

    F = zero()
    # time-dependent terms
    F += tau * dot(prev, u) * dx
    F -= tau * dot(x, u) * dx

    for i in range(ndim):
      for j in range(ndim):
        g = dot(ac[i], ac[j])
        F += dt * g * dot(Dx(x,i), Dx(u,j)) * dx

  elif formulation == 'nonlinear':
    ap, ac, Jp, Jc = deriv_vectors(x)

    F = zero()

    for i in range(ndim):
      for j in range(ndim):
        g = dot(ac[i], ac[j])
        F += g * dot(Dx(x,i), Dx(u,j)) * dx

    F = action(F, solution)
    J = derivative(F, solution, x)

elif maptype == 'wvd':

  if formulation == 'linear':
    ap, ac, Jp, Jc = deriv_vectors(prev)

    F = zero()

    # Coefficients
    alpha =  Dx(prev[0], 1) * Dx(prev[0], 1) \
           + Dx(prev[1], 1) * Dx(prev[1], 1)
    beta  =  Dx(prev[0], 0) * Dx(prev[0], 1) \
           + Dx(prev[1], 0) * Dx(prev[1], 1)
    gamma =  Dx(prev[0], 0) * Dx(prev[0], 0) \
           + Dx(prev[1], 0) * Dx(prev[1], 0)

    # Weak form
    F += -inner(x[0], u[0])*dx + \
          inner(prev[0], u[0])*dx - dt*( \
          Mc[0,0] * alpha * inner(grad(x[0])[0], grad(u[0])[0])*dx \
        - Mc[0,0] *  beta * inner(grad(x[0])[1], grad(u[0])[0])*dx \
        - Mc[0,0] *  beta * inner(grad(x[0])[0], grad(u[0])[1])*dx \
        + Mc[0,0] * gamma * inner(grad(x[0])[1], grad(u[0])[1])*dx \
        + Mc[0,0] * grad( beta)[1] * inner(grad(x[0])[0], u[0])*dx \
        - Mc[0,0] * grad(alpha)[0] * inner(grad(x[0])[0], u[0])*dx \
        + Mc[0,0] * grad( beta)[0] * inner(grad(x[0])[1], u[0])*dx \
        - Mc[0,0] * grad(gamma)[1] * inner(grad(x[0])[1], u[0])*dx \
        )
    F += -inner(x[1], u[1])*dx + \
          inner(prev[1], u[1])*dx - dt*( \
          Mc[1,1] * alpha * inner(grad(x[1])[0], grad(u[1])[0])*dx \
        - Mc[1,1] *  beta * inner(grad(x[1])[1], grad(u[1])[0])*dx \
        - Mc[1,1] *  beta * inner(grad(x[1])[0], grad(u[1])[1])*dx \
        + Mc[1,1] * gamma * inner(grad(x[1])[1], grad(u[1])[1])*dx \
        + Mc[1,1] * grad( beta)[1] * inner(grad(x[1])[0], u[1])*dx \
        - Mc[1,1] * grad(alpha)[0] * inner(grad(x[1])[0], u[1])*dx \
        + Mc[1,1] * grad( beta)[0] * inner(grad(x[1])[1], u[1])*dx \
        - Mc[1,1] * grad(gamma)[1] * inner(grad(x[1])[1], u[1])*dx \
        )

  elif formulation == 'nonlinear':
    ap, ac, Jp, Jc = deriv_vectors(x)

    F = zero()
    for i in range(ndim):
      for j in range(ndim):
        g = dot(ac[i], ac[j])
        F += dt * g * Mc[i,j] * dot(Dx(x,j), Dx(u,i)) * dx
        # F -= dt * Dx(g, i) * Mc[i,j] * dot(Dx(x, j), Dx(u, i)) * dx
    F = action(F, solution)
    J = derivative(F, solution, x)
    
elif maptype == 'mmpde':
  
  if formulation == 'linear':
    ap, ac, Jp, Jc = deriv_vectors(prev)
  elif formulation == 'nonlinear':
    ap, ac, Jp, Jc = deriv_vectors(x)
  
  G = Mc 
  a = {}
  for i in range(ndim):
    a[i] = {}
    for j in range(ndim): a[i][j] = dot(ac[i], G * ac[j])
  b = {}
  for i in range(ndim):
    b[i] = {}
    for j in range(ndim): b[i][j] = -dot(ac[i], Dx(G,j)*ac[j])
  p = 1.0/ (det(G))**0.5
  # create form = 0
  F = zero()
  
  if formulation == 'linear':
    # time derivative terms
    F += -tau * dot(x, u) * dx
    F +=  tau * dot(prev, u) * dx    
    # implicit
    for i in range(ndim):
      for j in range(ndim):
        F += dt * p * a[i][j] * dot(Dx(x, i), Dx(u, j)) * dx
        F += dt * p * b[i][j] * dot(Dx(x, i), u) * dx

#  elif formulation == 'nonlinear':
#    for i in range(ndim):
#      for j in range(ndim):
#        F += a[i][j] * dot(Dx(x, i), Dx(u, j)) * dx
#        F += b[i][j] * dot(Dx(x, i), u) * dx
#    F = action(F, solution)
#    J = derivative(F, solution, x)

else:
  raise ValueError('Unknown maptype')

if formulation == 'linear':
  problem = LinearVariationalProblem(lhs(F), rhs(F), solution, bcs=bcs)#bcs.values())
  solver = LinearVariationalSolver(problem,  solver_parameters={'pc_type': 'ilu',
                                                                #'ksp_rtol': 1e-19,
                                                                #'ksp_max_it' : 1000,
                                                                'ksp_converged_reason': False,
                                                                'ksp_monitor_true_residual': False,
                                                                'ksp_view': False}) # can add solver_parameters
#  solver.parameters['linear_solver'] = 'lu'
#elif formulation == 'nonlinear':
#  problem = NonlinearVariationalProblem(F, solution, bcs=bcs)#, J)
#  solver = NonlinearVariationalSolver(problem, solver_parameters={'snes_rtol': 1e-3,
#                                                                   'ksp_rtol': 1e-5,
#                                                                   'ksp_gmres_restart': 30,
#                                                                   'ksp_max_it' : 1000,
#                                                                   #'pc_type': 'sor',
#                                                                   'snes_monitor': True,
#                                                                   'snes_view': False,
#                                                                   'ksp_view': False,
#                                                                   'ksp_monitor_true_residual': True,
#                                                                   'snes_converged_reason': True,
#                                                                   'ksp_converged_reason': True})

Uspace = FunctionSpace(pmesh, 'CG', 1)
U = Function(Uspace).interpolate(U_exp)

entity_dofs = np.zeros(ndim+1, dtype=np.int32)
entity_dofs[0] = pmesh.geometric_dimension()
coordSection = pmesh._plex.createSection([1], entity_dofs, perm=pmesh.topology._plex_renumbering)
    
plex = pmesh._plex
vStart, vEnd = plex.getDepthStratum(0)
nbrVer = vEnd - vStart
    
dmCoords = pmesh.topology._plex.getCoordinateDM()
dmCoords.setDefaultSection(coordSection)    
    
with pmesh.coordinates.dat.vec_ro as coords:
  pmesh.topology._plex.setCoordinatesLocal(coords)
writeGmf(pmesh, 1, "boundary_ids", "cmesh", U, 1, "cmesh", coordSection)


for j in range(2):
  Uspace = FunctionSpace(pmesh, 'CG', 1)
  U = project(U_exp, Uspace)

  Mspace = TensorFunctionSpace(pmesh, 'CG', 1)
  M = as_tensor(((sqrt(1 + Dx(U,0)**2 + Dx(U,1)**2)/1.0, 0.0), 
      (0.0, sqrt(1 + Dx(U,0)**2 + Dx(U,1)**2)/1.0)))
  Mp = project(M, Mspace)
  
#  Uspace = FunctionSpace(pmesh, 'CG', 3)
#  U = project(U_exp, Uspace)
#  Mspace = TensorFunctionSpace(pmesh, 'CG', 1)
##  M = as_tensor(((sqrt(1 + Dx(U,0)**2 + Dx(U,1)**2)/1.0, 0.0), 
##      (0.0, sqrt(1 + Dx(U,0)**2 + Dx(U,1)**2)/1.0)))
#  Id_expr = Expression(((1,0),(0,1)))
#  Id = Function(Mspace).interpolate(Id_expr)
#  H = grad(grad(U))
#  Hp = project(H, Mspace)
#  absValHessian(pmesh, Hp)
#  Mp = Function(Mspace).assign(Hp+Id)

#  Mp = computeSteadyMetric(pmesh, U)

  Mc_prev = project(Mc, Mspace_c)
  Mc.dat.data[:] = Mp.dat.data

  tolerance = 1e-4
  print '---Solving mesh movement PDE'

  # direct solves
  if maptype in ['laplacian', 'candh']:
    solver.solve()

  # iterative solves
  else: # maptype in ['winslow', 'wvd']:
    if formulation == 'linear':
#      print 'tolerance = %1.2e' % tolerance
      diff = tolerance + 1.0
      i = 0
      while diff > tolerance:
        prev.assign(solution)
        solver.solve()
        diff = norm(Function(Wc).assign(solution-prev), 'l2') # TODO  was linf in code
        print 'difference = %1.3e' % diff
      
    elif formulation == 'nonlinear':
      solver.solve()

  if ndim == 1:
    pmesh.coordinates.dat.data[:,0] = solution.dat.data[:]
  elif ndim == 2:
    pmesh.coordinates.dat.data[:,0] = solution.dat.data[:,0]
    pmesh.coordinates.dat.data[:,1] = solution.dat.data[:,1]
  elif ndim == 3:
    pmesh.coordinates()[:,0] = solution.dat.data[:,0]
    pmesh.coordinates()[:,1] = solution.dat.data[:,1]
    pmesh.coordinates()[:,2] = solution.dat.data[:,2]



Uspace = FunctionSpace(pmesh, 'CG', 1)
U = Function(Uspace).interpolate(U_exp)

entity_dofs = np.zeros(ndim+1, dtype=np.int32)
entity_dofs[0] = pmesh.geometric_dimension()
coordSection = pmesh._plex.createSection([1], entity_dofs, perm=pmesh.topology._plex_renumbering)
    
plex = pmesh._plex
vStart, vEnd = plex.getDepthStratum(0)
nbrVer = vEnd - vStart
    
dmCoords = pmesh.topology._plex.getCoordinateDM()
dmCoords.setDefaultSection(coordSection)    
    
with pmesh.coordinates.dat.vec_ro as coords:
  pmesh.topology._plex.setCoordinatesLocal(coords)
writeGmf(pmesh, 1, "boundary_ids", "pmesh", U, 1, "pmesh", coordSection)