from firedrake import *
from math import pi as M_PI
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

option_dim = 3
n = 20
option_mmpde_ts = 0  # 0 for under-relax, 1 for time-stepping
option_theta_underrelax = 0.2
option_theta_ts = 0.5
option_sigma_smoothing = 0.001

if option_dim == 2 :
    mesh = UnitSquareMesh(n,n)
else :
    mesh = UnitCubeMesh(n,n,n)



########################  Solver stuff
    
T = 6

Q = FunctionSpace(mesh, "CG", 1)
V = VectorFunctionSpace(mesh, "CG", 2)
Vn = FunctionSpace(mesh, "CG", 1)
W = VectorFunctionSpace(mesh, "CG", 1)
M = TensorFunctionSpace(mesh, 'CG', 1)

u, v = TrialFunction(Q), TestFunction(Q)
u0, ucorr = Function(Q), Function(Q)     
c, c0, cn = Function(V), Function(V), Function(Vn) # velocity base and its norm
cmesh = Function(W) # velocity of the mes
cmeshp2 = Function(V)
cprint = Function(V)
        
zero = Constant(0)
bc = DirichletBC(Q, zero, 1)

time = Constant(0)
delta_t = Constant(0.1)
if option_dim == 2:
    cxExpr = "-sin(pi*x[0])*sin(pi*x[0])*sin(2*pi*x[1])" 
    cyExpr = "sin(2*pi*x[0])*sin(pi*x[1])*sin(pi*x[1])"
    c0.interpolate(Expression([cxExpr, cyExpr]))
else:
    cxExpr = "2*sin(pi*x[0])*sin(pi*x[0])*sin(2*pi*x[1])*sin(2*pi*x[2])"
    cyExpr = "-sin(2*pi*x[0])*sin(pi*x[1])*sin(pi*x[1])*sin(2*pi*x[2])"
    czExpr = "-sin(2*pi*x[0])*sin(2*pi*x[1])*sin(pi*x[2])*sin(pi*x[2])"
    c0.interpolate(Expression([cxExpr, cyExpr, czExpr]))
c = c0*cos(2*pi*time/T)

if option_dim == 2: cmesh.interpolate(Expression((0,0)))
else : cmesh.interpolate(Expression((0,0,0)))

c = c-cmesh

a = v*u*dx(degree=3) + 0.5*delta_t*(v*dot(c, grad(u))*dx(degree=3))
L = v*u0*dx(degree=3) - 0.5*delta_t*(v*dot(c, grad(u0))*dx(degree=3))
# Add SUPG stabilization terms
ra = u + 0.5*delta_t*(dot(c, grad(u)))
rL = u0 - 0.5*delta_t*(dot(c, grad(u0)))
cnorm = sqrt(dot(c, c))
h = Constant(1./n)
a += (h/(2.*cnorm))*dot(c, grad(v))*ra*dx(degree=3)
L += (h/(2.*cnorm))*dot(c, grad(v))*rL*dx(degree=3)

A_prob = LinearVariationalProblem(a,L,u0,bcs=bc, constant_jacobian=False)
pc = 'ilu'
A_solv = LinearVariationalSolver(A_prob, solver_parameters={'pc_type': pc,
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': True,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})




######################## Moving mesh stuff 


mesh_crd0 = Function(W)

mesh_crd0.dat.data[:] = mesh.coordinates.dat.data
t = 0

####### Structures for the MMDPE
if option_dim == 2 :
    mesh_c = UnitSquareMesh(n,n)
else :
    mesh_c = UnitCubeMesh(n,n,n)

theta = option_theta_underrelax

U_c = FunctionSpace(mesh_c, 'CG', 1)
V_c = VectorFunctionSpace(mesh_c, 'CG', 1)
W_c = VectorFunctionSpace(mesh_c, 'CG', 1)

phi = TestFunction(V_c)
x_new = TrialFunction(V_c)
x = Function(V_c)
u_c = Function(U_c)
w_c = Function(U_c)

if option_dim == 2 : bc_mmpde_exp = Expression(('x[0]', 'x[1]'))
else: bc_mmpde_exp = Expression(('x[0]', 'x[1]', 'x[2]'))
bcs_mmpde = DirichletBC(V_c, bc_mmpde_exp, "on_boundary")

if option_dim == 2 : M_c = w_c*as_tensor([[1,0],[0,1]])
else :M_c = w_c*as_tensor([[1,0,0],[0,1, 0],[0,0,1]])

if (option_mmpde_ts == 0) :
    #F = inner(M_c, dot(transpose(grad(x_new)),grad(phi)))*dx + Constant(0)*phi[0]*dx(degree=0)
    F = inner(dot(M_c,grad(x_new)), grad(phi))*dx(degree=2) + Constant(0)*phi[0]*dx(degree=0)

    M_prob = LinearVariationalProblem(lhs(F), rhs(F), x, bcs=bcs_mmpde, constant_jacobian=False)
    M_solv = LinearVariationalSolver(M_prob, solver_parameters={'pc_type': 'ilu',
                                                                'ksp_max_it' : 1500,
                                                                'ksp_converged_reason': True,
                                                                'ksp_monitor_true_residual': False,
                                                                'ksp_view': False})
elif (option_mmpde_ts == 1) :
    a = Constant(1)
    delta_t2 = Constant(1)
    theta_ts = option_theta_ts
    F_bis = dot(x_new-x,phi)*dx + delta_t2*inner(dot(M_c,grad(theta_ts*x_new +(1-theta_ts)*x)),grad(phi))*dx \
                          + delta_t2*a*inner(grad(x_new-x),grad(phi))*dx


    M_prob_bis = LinearVariationalProblem(lhs(F_bis), rhs(F_bis), x, bcs=bcs_mmpde, constant_jacobian=False)
    M_solv_bis = LinearVariationalSolver(M_prob_bis, solver_parameters={'pc_type': 'ilu',
                                                                'ksp_max_it' : 1500,
                                                                'ksp_converged_reason': True,
                                                                'ksp_monitor_true_residual': False,
                                                                'ksp_view': False})
    


####### Structures to smooth the monitor function
sigma = Constant(option_sigma_smoothing)

Mn = TrialFunction(U_c)
um = TestFunction(U_c)

bcs_smoothMF = DirichletBC(U_c, w_c, 'on_boundary')

H = Mn*um*dx + sigma*dot(grad(Mn), grad(um))*dx - w_c*um*dx

S_prob = LinearVariationalProblem(lhs(H), rhs(H), w_c, bcs=bcs_smoothMF, constant_jacobian=False)
S_solv = LinearVariationalSolver(S_prob);, solver_parameters={'pc_type': 'ilu',
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': False,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})
     




########################  Initial step


print "############################################"
print "###     Beginning of the real stuff      ###"
print "############################################"


if option_dim == 2:
    U_ini_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.75)*(x[1]-0.75)-0.15*0.15))", alpha = 5000)
else: 
    U_ini_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.35)*(x[0]-0.35) + (x[1]-0.35)*(x[1]-0.35) + (x[2]-0.35)*(x[2]-0.35)-0.15*0.15))", alpha = 5000)
u0.interpolate(U_ini_exp)


if option_mmpde_ts == 0:
    nt = 25
elif option_mmpde_ts == 1:
    nt = 100
    np.copyto(x.dat.data, mesh.coordinates.dat.data)
outfilemm = File("mm.pvd")
outfilemm.write(u0) 
for i in range(nt):
    u_c.dat.data[:] = u0.dat.data 
    if option_dim == 2 : w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1)))
    else : w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1) + Dx(u_c,2)*Dx(u_c,2)))

    print "DEBUG   Here"
    ###  Smooth monitor function
    S_solv.solve()

    print "DEBUG   There"

    ###  Solve MMPDE
    if option_mmpde_ts == 0:
        M_solv.solve()    
        res = np.linalg.norm(theta*abs(mesh.coordinates.dat.data-x.dat.data))
        res = MPI.COMM_WORLD.allreduce(res, op=MPI.SUM)
        np.copyto(mesh.coordinates.dat.data, theta*x.dat.data + (1-theta)*mesh.coordinates.dat.data)
    elif option_mmpde_ts == 1:
        a_val = w_c.dat.data.max()
        a.assign(a_val)
        M_solv_bis.solve()    
        res = np.linalg.norm(abs(mesh.coordinates.dat.data-x.dat.data))
        res = MPI.COMM_WORLD.allreduce(res, op=MPI.SUM)
        np.copyto(mesh.coordinates.dat.data, x.dat.data)
        
    if rank == 0: print "###  Mesh iteration: %d,  convergence residual: %1.2e" %(i, res)

    ### update functions
    u0.interpolate(U_ini_exp)
    outfilemm.write(u0) 

    if res < 0.005: break




outfile = File("bubblemm.pvd")
outfile.write(u0) 

t = 0
ite = 1

#dt = 0.05/n
altMin = Function(Q)
if option_dim == 2:
    altMin.interpolate(2*CellVolume(mesh)/MaxCellEdgeLength(mesh))
else:
    altMin.interpolate(Circumradius(mesh)*MinCellEdgeLength(mesh)/MaxCellEdgeLength(mesh))
dt = 10*altMin.dat.data.min()
dt = MPI.COMM_WORLD.allreduce(dt, op=MPI.MIN)
print "DEBUG(%d)  dt: %1.3e" %(rank,dt)
iteSav = max(1, int(1.5/(400*dt)))

while t <= 1.5: #0.5*T:
    
    if rank == 0: print "##########  Ite: %d   t: %1.2e --> %1.2e" % (ite, t, t+dt)

    ###### Solve the equation
    time.assign(t)
    delta_t.assign(dt)

    A_solv.solve()

    ucorr.dat.data[...] = np.maximum(u0.dat.data, 0)
    ucorr.dat.data[...] = np.minimum(ucorr.dat.data, 1)
    u0.assign(ucorr)

    # this needs to be done *before* moving the mesh
    if option_dim == 2 : c0.interpolate(Expression([cxExpr, cyExpr]))  
    else : c0.interpolate(Expression([cxExpr, cyExpr, czExpr]))


    ####### write to file
    if (ite%iteSav == 0) : outfile.write(u0) 


    ##### Move the mesh
    for i in range(1):
        u_c.dat.data[:] = u0.dat.data 
        if option_dim == 2 : w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1)))
        else : w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1) + Dx(u_c,2)*Dx(u_c,2)))

        ###  Smooth monitor function
        S_solv.solve()

        ###  Solve MMPDE
        if option_mmpde_ts == 0:
            M_solv.solve()    
            res = np.linalg.norm(theta*abs(mesh.coordinates.dat.data-x.dat.data))
            res = MPI.COMM_WORLD.allreduce(res, op=MPI.SUM)
            np.copyto(cmesh.dat.data, theta*(x.dat.data-mesh.coordinates.dat.data)/dt)
            np.copyto(mesh.coordinates.dat.data, theta*x.dat.data + (1-theta)*mesh.coordinates.dat.data)
        elif option_mmpde_ts == 1:
            a_val = w_c.dat.data.max()
            a.assign(a_val)
            M_solv_bis.solve()    
            res = np.linalg.norm(abs(mesh.coordinates.dat.data-x.dat.data))
            res = MPI.COMM_WORLD.allreduce(res, op=MPI.SUM)
            np.copyto(cmesh.dat.data, (x.dat.data-mesh.coordinates.dat.data)/dt)
            np.copyto(mesh.coordinates.dat.data, x.dat.data)
            
        if rank == 0: print "###  Mesh iteration: %d,  convergence residual: %1.2e" %(i, res)

    ####### advance in time
    t += dt
    ite += 1
