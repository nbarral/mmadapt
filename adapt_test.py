from firedrake import *
import numpy as np
import MMPDE as ms

# =============================================================================
# Problem options
# =============================================================================

meshfile = 'square.msh'

# Gaussian function for deformation of bottom boundary
# A: height, B: position, C: standard deviation
bc_exp = Expression(
  ('x[0]','-1.0+A*exp(-pow((x[0]-B),2)/(2.0*pow(C,2)))'),
  A=0.0, B=0.0, C=0.3)

U_exp = Expression( 
  '1.0 - 100*A*tanh( 10*(pow(x[0],2) + pow(x[1],2)) - 20/8.0 )', A=0.005)

maptype = 'wvd'

# =============================================================================
# Mesh movement problem setup
# =============================================================================
cmesh = Mesh(meshfile)
pmesh = Mesh(meshfile)

MM = ms.MeshMap(
  meshfile, 
  maptype = maptype, 
  bndy_labels = "boundary_ids", 
  formulation = 'linear')

for j in range(50):
  Uspace = FunctionSpace(MM.pmesh, 'CG', 1)
  U = project(U_exp, Uspace)
  Mspace = TensorFunctionSpace(MM.pmesh, 'CG', 1)
  M = as_tensor(((sqrt(1 + Dx(U,0)**2 + Dx(U,1)**2)/1.0, 0.0), 
      (0.0, sqrt(1 + Dx(U,0)**2 + Dx(U,1)**2)/1.0)))
  Mp = project(M, Mspace)

  MM.update_monitor(Mp)

  # =============================================================================
  # Solve
  # =============================================================================
  MM.solve(tolerance = 1e-3)

  # =============================================================================
  # Postprocessing
  # =============================================================================

  MM.update_pmesh()

pmesh = MM.pmesh
