from firedrake import *
import numpy as np
from math import pi as M_PI



n = 100

####### Structures for the advection problem

mesh = UnitSquareMesh(n,n)
    
T = 6

Q = FunctionSpace(mesh, "CG", 1)
U_p = Q
V = VectorFunctionSpace(mesh, "CG", 2)
Vn = FunctionSpace(mesh, "CG", 1)
W = VectorFunctionSpace(mesh, "CG", 1)
M = TensorFunctionSpace(mesh, 'CG', 1)

u, v = TrialFunction(Q), TestFunction(Q)
u0, ucorr = Function(Q), Function(Q)     
c0, cn = Function(V), Function(Vn) # velocity base and its norm
cmesh = Function(W) # velocity of the mesh


        
zero = Constant(0)
bc = DirichletBC(Q, zero, 1)

time = Constant(0)
delta_t = Constant(0.1)
cxExpr = "-sin(pi*x[0])*sin(pi*x[0])*sin(2*pi*x[1])" 
cyExpr = "sin(2*pi*x[0])*sin(pi*x[1])*sin(pi*x[1])"
#c0.interpolate(Expression([cxExpr, cyExpr]))
#c = c0*cos(2*pi*time/T)

c0.interpolate(Expression([0, "-0.25"]))
c = c0

cmesh.interpolate(Expression((0,0)))

c = c-cmesh


a = v*u*dx(degree=3) + 0.5*delta_t*(v*dot(c, grad(u))*dx(degree=3))
L = v*u0*dx(degree=3) - 0.5*delta_t*(v*dot(c, grad(u0))*dx(degree=3))
# Add SUPG stabilization terms
ra = u + 0.5*delta_t*(dot(c, grad(u)))
rL = u0 - 0.5*delta_t*(dot(c, grad(u0)))
cnorm = sqrt(dot(c, c))
h = Constant(1./n)
a += (h/(2.*cnorm))*dot(c, grad(v))*ra*dx(degree=3)
L += (h/(2.*cnorm))*dot(c, grad(v))*rL*dx(degree=3)

A_prob = LinearVariationalProblem(a,L,u0,bcs=bc, constant_jacobian=False)
pc = 'ilu'
A_solv = LinearVariationalSolver(A_prob, solver_parameters={'pc_type': pc,
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': True,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})




####### Structures for the MMDPE
mesh_c = UnitSquareMesh(n,n)
mmpde_order = 1
theta = 0.2


U_c = FunctionSpace(mesh_c, 'CG', mmpde_order)
V_c = VectorFunctionSpace(mesh_c, 'CG', mmpde_order)
W_c = VectorFunctionSpace(mesh_c, 'CG', 1)

phi = TestFunction(V_c)
x = Function(V_c)
x_new = TrialFunction(V_c)
u_c = Function(U_c)
w_c = Function(U_c)
y = Function(W_c)

bc_mmpde_exp = Expression(('x[0]', 'x[1]'))
bcs_mmpde = DirichletBC(V_c, bc_mmpde_exp, "on_boundary")

M_c = w_c*as_tensor([[1,0],[0,1]])
F = inner(M_c, dot(transpose(grad(x_new)),grad(phi)))*dx + Constant(0)*phi[0]*dx(degree=0)

M_prob = LinearVariationalProblem(lhs(F), rhs(F), x, bcs=bcs_mmpde, constant_jacobian=False)
M_solv = LinearVariationalSolver(M_prob, solver_parameters={'pc_type': 'lu',
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': False,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})


mesh_tmp = UnitSquareMesh(n,n)
u1 = Function(FunctionSpace(mesh_tmp, 'CG', 1))

w_p = Function(U_p)


####### Structures to smooth the monitor function
sigma = Constant(0.001)

Mn = TrialFunction(U_c)
um = TestFunction(U_c)

bcs_smoothMF = DirichletBC(U_c, w_c, 'on_boundary')

H = Mn*um*dx + sigma*dot(grad(Mn), grad(um))*dx - w_c*um*dx

S_prob = LinearVariationalProblem(lhs(H), rhs(H), w_c, bcs=bcs_smoothMF, constant_jacobian=False)
S_solv = LinearVariationalSolver(S_prob, solver_parameters={'pc_type': 'lu',
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': False,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})



######## Init step

U_ini_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.75)*(x[1]-0.75)-0.15*0.15))", alpha = 5000)

nt = 25
u0.interpolate(U_ini_exp)
for i in range(nt):

    u_c.dat.data[:] = u0.dat.data 
    w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1)))
#    w_p.interpolate(sqrt(1+ Dx(u0,0)*Dx(u0,0) + Dx(u0,1)*Dx(u0,1)))
#    np.copyto(w_c.dat.data, w_p.dat.data)

    ###  Smooth monitor function
    S_solv.solve()

    ###  Solve MMPDE
    M_solv.solve()    
    y.interpolate(x)
    res = np.linalg.norm(abs(mesh.coordinates.dat.data-y.dat.data))
    print "###  Mesh iteration: %d,  convergence residual: %1.2e" %(i, res)
    if res < 0.0005 : break
    mesh.coordinates.dat.data[:,0] = theta*y.dat.data[:,0] + (1-theta)*mesh.coordinates.dat.data[:,0]
    mesh.coordinates.dat.data[:,1] = theta*y.dat.data[:,1] + (1-theta)*mesh.coordinates.dat.data[:,1]

    ### update functions
    u0 = Function(functionspaceimpl.WithGeometry(u0.function_space(), mesh), val=u0.topological)

    u0.interpolate(U_ini_exp)


ndim = 2
entity_dofs = np.zeros(ndim+1, dtype=np.int32)
entity_dofs[0] = mesh.geometric_dimension()
coordSection = mesh._plex.createSection([1], entity_dofs, perm=mesh.topology._plex_renumbering)
    
plex = mesh._plex
vStart, vEnd = plex.getDepthStratum(0)
nbrVer = vEnd - vStart
    
dmCoords = mesh.topology._plex.getCoordinateDM()
dmCoords.setDefaultSection(coordSection)    
    
with mesh.coordinates.dat.vec_ro as coords:
    mesh.topology._plex.setCoordinatesLocal(coords)
writeGmf(mesh, 1, "boundary_ids", "bubble.%d" % 0, u0, 1, "bubble.%d" % 0, coordSection)

mesh_crd0 = Function(W)
mesh_crd0.dat.data[:] = mesh.coordinates.dat.data


np.copyto(mesh_tmp.coordinates.dat.data, mesh.coordinates.dat.data)
coordSection_tmp = mesh_tmp._plex.createSection([1], entity_dofs, perm=mesh_tmp.topology._plex_renumbering)    
dmCoords_tmp = mesh_tmp.topology._plex.getCoordinateDM()
dmCoords_tmp.setDefaultSection(coordSection_tmp) 
with mesh_tmp.coordinates.dat.vec_ro as coords:
    mesh_tmp.topology._plex.setCoordinatesLocal(coords)
writeGmf(mesh_tmp, 1, "boundary_ids", "bubble_tmp.%d" % 0, None, 0, "", coordSection_tmp)

####  Update all other functions

c0 = Function(functionspaceimpl.WithGeometry(c0.function_space(), mesh), val=c0.topological)
cn = Function(functionspaceimpl.WithGeometry(cn.function_space(), mesh), val=cn.topological)
#c0.interpolate(Expression([cxExpr, cyExpr]))

outFile = File("bubble.pvd")
outFile.write(u0)

######## time-marching solution

t = 0
ite = 1

tEnd = 0.5
dt = 0.1/n
crdOld = np.empty_like(mesh.coordinates.dat.data)

while t < tEnd:

    #######  Solve physical problem
    time.assign(t)
    delta_t.assign(dt)

    print "##########  Ite: %d   t: %1.2e --> %1.2e" % (ite, t, t+dt)

    #A_solv.solve()
    U_tim_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.75+time)*(x[1]-0.75+time)-0.15*0.15))", alpha = 5000, time=t)
    u0.interpolate(U_tim_exp)

    ucorr.dat.data[...] = np.maximum(u0.dat.data, 0)
    ucorr.dat.data[...] = np.minimum(ucorr.dat.data, 1)
    u0.assign(ucorr)

#    c0.interpolate(Expression([cxExpr, cyExpr]))


    iteSav = 10
    if (ite%iteSav == 0) :
        with mesh.coordinates.dat.vec_ro as coords:
            mesh.topology._plex.setCoordinatesLocal(coords)
        writeGmf(mesh, 1, "boundary_ids", "bubble.%d" % (ite/iteSav), u0, 1, "bubble.%d" % (ite/iteSav), coordSection)
    


    #######  Move the mesh to the new position

    u_c.dat.data[:] = u0.dat.data 
    w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1)))

    ###  Smooth monitor function
    S_solv.solve()

    ###  Solve MMPDE
    M_solv.solve()    
    y.interpolate(x)

#    np.copyto(mesh.coordinates.dat.data, y.dat.data)
    np.copyto(mesh_tmp.coordinates.dat.data, y.dat.data)

    iteSav = 10
    if (ite%iteSav == 0) :
        with mesh_tmp.coordinates.dat.vec_ro as coords:
            mesh_tmp.topology._plex.setCoordinatesLocal(coords)
        writeGmf(mesh_tmp, 1, "boundary_ids", "bubble_tmp.%d" % (ite/iteSav), None, 0, "", coordSection_tmp)

#    alpha = 0.2
#    T_mm = 1
#    y.dat.data[:,0] = newCrd = mesh_crd0.dat.data[:,0] + alpha * np.sin(M_PI*mesh_crd0.dat.data[:,0]) * np.sin(2*M_PI*mesh_crd0.dat.data[:,1]) * sin(2*2*M_PI/T_mm *t)

#    np.copyto(cmesh.dat.data,(y.dat.data-mesh.coordinates.dat.data)/dt)
#    np.copyto(mesh.coordinates.dat.data, y.dat.data)

    outFile.write(u0)    

    ### update function
    u0 = Function(functionspaceimpl.WithGeometry(u0.function_space(), mesh), val=u0.topological)
    c0 = Function(functionspaceimpl.WithGeometry(c0.function_space(), mesh), val=c0.topological)
    cmesh = Function(functionspaceimpl.WithGeometry(cmesh.function_space(), mesh), val=cmesh.topological)


    ###### Write new mesh

#    iteSav = 1
#    if (ite%iteSav == 0) :
#        with mesh.coordinates.dat.vec_ro as coords:
#            mesh.topology._plex.setCoordinatesLocal(coords)
#        writeGmf(mesh, 1, "boundary_ids", "bubble_after.%d" % (ite/iteSav), u0, 1, "bubble_after.%d" % (ite/iteSav), coordSection)


    ###### Advance in time

    t += delta_t.dat.data[0]
    ite += 1

#    break