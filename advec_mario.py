from firedrake import *
from math import pi as M_PI
import numpy as np

n = 75

mesh = UnitSquareMesh(n,n)
    



########################  Solver stuff
    
T = 6

Q = FunctionSpace(mesh, "CG", 1)
V = VectorFunctionSpace(mesh, "CG", 2)
Vn = FunctionSpace(mesh, "CG", 1)
W = VectorFunctionSpace(mesh, "CG", 1)
M = TensorFunctionSpace(mesh, 'CG', 1)

u, v = TrialFunction(Q), TestFunction(Q)
u0, ucorr = Function(Q), Function(Q)     
c, c0, cn = Function(V), Function(V), Function(Vn) # velocity base and its norm
cmesh = Function(W) # velocity of the mes
cmeshp2 = Function(V)
cprint = Function(V)
        
zero = Constant(0)
bc = DirichletBC(Q, zero, 1)

time = Constant(0)
delta_t = Constant(0.1)
cxExpr = "-sin(pi*x[0])*sin(pi*x[0])*sin(2*pi*x[1])" 
cyExpr = "sin(2*pi*x[0])*sin(pi*x[1])*sin(pi*x[1])"
c0.interpolate(Expression([cxExpr, cyExpr]))
c = c0*cos(2*pi*time/T)

cmesh.interpolate(Expression((0,0)))

c = c-cmesh

a = v*u*dx(degree=3) + 0.5*delta_t*(v*dot(c, grad(u))*dx(degree=3))
L = v*u0*dx(degree=3) - 0.5*delta_t*(v*dot(c, grad(u0))*dx(degree=3))
# Add SUPG stabilization terms
ra = u + 0.5*delta_t*(dot(c, grad(u)))
rL = u0 - 0.5*delta_t*(dot(c, grad(u0)))
cnorm = sqrt(dot(c, c))
h = Constant(1./n)
a += (h/(2.*cnorm))*dot(c, grad(v))*ra*dx(degree=3)
L += (h/(2.*cnorm))*dot(c, grad(v))*rL*dx(degree=3)

A_prob = LinearVariationalProblem(a,L,u0,bcs=bc, constant_jacobian=False)
pc = 'ilu'
A_solv = LinearVariationalSolver(A_prob, solver_parameters={'pc_type': pc,
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': True,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})




######################## Moving mesh stuff 


mesh_crd0 = Function(W)

mesh_crd0.dat.data[:] = mesh.coordinates.dat.data

alpha = 0.
T_mm = 1
t = 0

####### Structures for the MMDPE
mesh_c = mesh #UnitSquareMesh(n,n)
theta = 0.2


U_c = FunctionSpace(mesh_c, 'CG', 1)
V_c = VectorFunctionSpace(mesh_c, 'CG', 1)
W_c = VectorFunctionSpace(mesh_c, 'CG', 1)

phi = TestFunction(V_c)
deltax_new = TrialFunction(V_c)
deltax = Function(V_c)
u_c = Function(U_c)
w_c = Function(U_c)

bc_mmpde_exp = Expression((0, 0))
bcs_mmpde = DirichletBC(V_c, bc_mmpde_exp, "on_boundary")

#M_c = w_c*as_tensor([[1,0],[0,1]])
a = inner(w_c*grad(deltax_new), grad(phi))*dx
#L = -w_c*div(phi)*dx
L = dot(grad(w_c), phi)*dx

M_prob = LinearVariationalProblem(a, L, deltax, bcs=bcs_mmpde, constant_jacobian=False)
M_solv = LinearVariationalSolver(M_prob, solver_parameters={'pc_type': 'lu',
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': False,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})
    
w_p = Function(Q)


####### Structures to smooth the monitor function
sigma = Constant(0.02)

Mn = TrialFunction(U_c)
um = TestFunction(U_c)

bcs_smoothMF = DirichletBC(U_c, w_c, 'on_boundary')

H = Mn*um*dx + sigma*dot(grad(Mn), grad(um))*dx - w_c*um*dx

S_prob = LinearVariationalProblem(lhs(H), rhs(H), w_c, bcs=bcs_smoothMF, constant_jacobian=False)
S_solv = LinearVariationalSolver(S_prob, solver_parameters={'pc_type': 'lu',
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': False,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})
     




########################  Initial step

U_ini_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.75)*(x[1]-0.75)-0.15*0.15))", alpha = 5000)
u0.interpolate(U_ini_exp)

outfile_mm = File("mmesh.pvd")
outfile_mm.write(u0) 

nt = 30
for i in range(nt):

#    u_c.dat.data[:] = u0.dat.data 
#    w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1)))
    w_p.interpolate(sqrt(1+ Dx(u0,0)*Dx(u0,0) + Dx(u0,1)*Dx(u0,1)))
    np.copyto(w_c.dat.data, w_p.dat.data)

    ###  Smooth monitor function
    S_solv.solve()

    ###  Solve MMPDE
    M_solv.solve()    
    res = np.linalg.norm(abs(deltax.dat.data))
    print "###  Mesh iteration: %d,  convergence residual: %1.2e" %(i, res)
    if res < 0.005 : break
    mesh.coordinates.dat.data[:,0] += theta*deltax.dat.data[:,0]
    mesh.coordinates.dat.data[:,1] += theta*deltax.dat.data[:,1]

    ### update functions
    u0.interpolate(U_ini_exp)
    outfile_mm.write(u0) 

    

outfile = File("bubblemm.pvd")
outfile.write(u0) 

exit(12)

t = 0
ite = 1

#dt = 0.05/n
altMin = Function(Q)
altMin.interpolate(2*CellVolume(mesh)/MaxCellEdgeLength(mesh))
dt = 2*altMin.dat.data.min()

while t <= 1.5: #0.5*T:
    
    print "##########  Ite: %d   t: %1.2e --> %1.2e" % (ite, t, t+dt)

    ###### Solve the equation
    time.assign(t)
    delta_t.assign(dt)

    A_solv.solve()

    ucorr.dat.data[...] = np.maximum(u0.dat.data, 0)
    ucorr.dat.data[...] = np.minimum(ucorr.dat.data, 1)
    u0.assign(ucorr)

    c0.interpolate(Expression([cxExpr, cyExpr]))  # this needs to be done *before* moving the mesh

    ####### write to file
    iteSav = int(1.5/(400*dt))
    if (ite%iteSav == 0) :  
        outfile.write(u0) 


    ##### Move the mesh
    cmesh.interpolate(Expression([0,0]))
    nt = 1
#    if ite%2 : nt = 1
#    else : nt = 0
    for i in range(nt):

        u_c.dat.data[:] = u0.dat.data 
        w_c.interpolate(sqrt(1+ Dx(u_c,0)*Dx(u_c,0) + Dx(u_c,1)*Dx(u_c,1)))

        ###  Smooth monitor function
        S_solv.solve()

        ###  Solve MMPDE
        M_solv.solve() 
        res = np.linalg.norm(abs(mesh.coordinates.dat.data-x.dat.data))
        print "###  Mesh iteration: %d,  convergence residual: %1.2e" %(i, res)
        np.copyto(cmesh.dat.data, theta*(x.dat.data-mesh.coordinates.dat.data)/dt)
        mesh.coordinates.dat.data[:,0] = theta*x.dat.data[:,0] + (1-theta)*mesh.coordinates.dat.data[:,0]
        mesh.coordinates.dat.data[:,1] = theta*x.dat.data[:,1] + (1-theta)*mesh.coordinates.dat.data[:,1]

        ### update functions
        u0 = Function(functionspaceimpl.WithGeometry(u0.function_space(), mesh), val=u0.topological)
    

    ####### Update functions
    c0 = Function(functionspaceimpl.WithGeometry(c0.function_space(), mesh), val=c0.topological)
    cn = Function(functionspaceimpl.WithGeometry(cn.function_space(), mesh), val=cn.topological)

    ####### advance in time
    t += dt
    ite += 1
