from firedrake import *
import numpy as np
from numpy import linalg as LA

import time, gc



def absValHessian(mesh, H):

     for iVer in range(mesh.topology.num_vertices()):
        hesLoc = H.dat.data[iVer]
        meanDiag = 0.5*(hesLoc[0][1] + hesLoc[1][0]);
        hesLoc[0][1] = meanDiag
        hesLoc[1][0] = meanDiag
        lbd, v = LA.eig(hesLoc)
        lbd1 = max(abs(lbd[0]), 1e-10)
        lbd2 = max(abs(lbd[1]), 1e-10)
        det = lbd1*lbd2
        v1 = v[0]
        v2 = v[1]
        H.dat.data[iVer][0,0] = lbd1*v1[0]*v1[0] + lbd2*v2[0]*v2[0];
        H.dat.data[iVer][0,1] = lbd1*v1[0]*v1[1] + lbd2*v2[0]*v2[1];
        H.dat.data[iVer][1,0] = H.dat.data[iVer][0,1]
        H.dat.data[iVer][1,1] = lbd1*v1[1]*v1[1] + lbd2*v2[1]*v2[1];



def computeSteadyMetric(mesh, sol) :
    
    dim = 2
    p = 2
    N = 1000
    use = N
    umin = 0.01
    a = 1000
    usa2 = 1./(a*a)
    hmin = 0.01
    ushmin2 = 1./(hmin*hmin)
    hmax = 0.5
    ushmax2 = 1./(hmax*hmax)
    snes_rtol = 1e-2
    ksp_rtol = 1e-5
    

    MetricSpace = TensorFunctionSpace(mesh, 'CG', 1)

    sigma = TestFunction(MetricSpace)
    H = Function(MetricSpace)
    n = FacetNormal(mesh)
    Lh = inner(sigma, H)*dx + inner(div(sigma), grad(sol))*dx
    if dim == 2:
        Lh -= (sigma[0, 1]*n[1]*sol.dx(0) + sigma[1, 0]*n[0]*sol.dx(1))*ds
    else :
        Lh -= (sigma[0,0]*sol.dx(0)*n[0] + sigma[1,0]*sol.dx(1)*n[0] + sigma[2,0]*sol.dx(2)*n[0] \
            +  sigma[0,1]*sol.dx(0)*n[1] + sigma[1,1]*sol.dx(1)*n[1] + sigma[2,1]*sol.dx(2)*n[1] \
            +  sigma[0,2]*sol.dx(0)*n[2] + sigma[1,2]*sol.dx(1)*n[2] + sigma[2,2]*sol.dx(2)*n[2])*ds
        print "## Warning: Variational form for 3D hessian computation still under heavy work"    
    H_prob = NonlinearVariationalProblem(Lh, H)
    H_solv = NonlinearVariationalSolver(H_prob, solver_parameters={'snes_rtol': snes_rtol,
                                                                   'ksp_rtol': ksp_rtol,
                                                                   'ksp_gmres_restart': 20,
                                                                   'pc_type': 'sor',
                                                                   'snes_monitor': True,
                                                                   'snes_view': False,
                                                                   'ksp_monitor_true_residual': False,
                                                                   'snes_converged_reason': True,
                                                                   'ksp_converged_reason': True})

    H_solv.solve()


    
    # computation of the metric of Loseille 2011
    detHes = Function(FunctionSpace(mesh, 'CG', 1))
    for iVer in range(mesh.topology.num_vertices()):
        hesLoc = H.dat.data[iVer]
        meanDiag = 0.5*(hesLoc[0][1] + hesLoc[1][0]);
        hesLoc[0][1] = meanDiag
        hesLoc[1][0] = meanDiag
        lbd, v = LA.eig(hesLoc)
        lbd1 = max(abs(lbd[0]), 1e-10)
        lbd2 = max(abs(lbd[1]), 1e-10)
        det = lbd1*lbd2
        v1 = v[0]
        v2 = v[1]
        H.dat.data[iVer][0,0] = lbd1*v1[0]*v1[0] + lbd2*v2[0]*v2[0] + 1;
        H.dat.data[iVer][0,1] = lbd1*v1[0]*v1[1] + lbd2*v2[0]*v2[1];
        H.dat.data[iVer][1,0] = H.dat.data[iVer][0,1]
        H.dat.data[iVer][1,1] = lbd1*v1[1]*v1[1] + lbd2*v2[1]*v2[1] + 1;
#        H.dat.data[iVer] *= pow(det, -1./(2*p+2))
#        detHes.dat.data[iVer] = pow(det, p/(2.*p+2))


    return H
    
    detHesInt = assemble(detHes*dx)
    glbNrmCof = (N/detHesInt)
    H *= glbNrmCof
    
    for iVer in range(mesh.topology.num_vertices()):
        lbd, v = LA.eig(H.dat.data[iVer])
        # truncation of eigenvalues
        lbd1 = min(ushmin2, max(ushmax2,lbd[0]))
        lbd2 = min(ushmin2, max(ushmax2,lbd[1]))
        maxLbd = max(lbd1, lbd2)
        lbd1 = max(lbd1, usa2*maxLbd)
        lbd2 = max(lbd2, usa2*maxLbd)
        # reconstruction of |Hu|
        v1 = v[0]
        v2 = v[1]
        H.dat.data[iVer][0,0] = lbd1*v1[0]*v1[0] + lbd2*v2[0]*v2[0];
        H.dat.data[iVer][0,1] = lbd1*v1[0]*v1[1] + lbd2*v2[0]*v2[1];
        H.dat.data[iVer][1,0] = H.dat.data[iVer][0,1]
        H.dat.data[iVer][1,1] = lbd1*v1[1]*v1[1] + lbd2*v2[1]*v2[1];

    return H

