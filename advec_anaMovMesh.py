from firedrake import *
from math import pi as M_PI
import numpy as np

n = 200

mesh = UnitSquareMesh(n,n)
    

########################  Solver stuff
    
T = 6

Q = FunctionSpace(mesh, "CG", 1)
V = VectorFunctionSpace(mesh, "CG", 2)
Vn = FunctionSpace(mesh, "CG", 1)
W = VectorFunctionSpace(mesh, "CG", 1)
M = TensorFunctionSpace(mesh, 'CG', 1)

u, v = TrialFunction(Q), TestFunction(Q)
u0, ucorr = Function(Q), Function(Q)     
c, c0, cn = Function(V), Function(V), Function(Vn) # velocity base and its norm
cmesh = Function(W) # velocity of the mes
cmeshp2 = Function(V)
cprint = Function(V)

        
zero = Constant(0)
bc = DirichletBC(Q, zero, 1)

time = Constant(0)
delta_t = Constant(0.1)
cxExpr = "-sin(pi*x[0])*sin(pi*x[0])*sin(2*pi*x[1])" 
cyExpr = "sin(2*pi*x[0])*sin(pi*x[1])*sin(pi*x[1])"
c0.interpolate(Expression([cxExpr, cyExpr]))
c = c0*cos(2*pi*time/T)

#c0.interpolate(Expression([0, "-0.25"]))
#c = c0
#c0.interpolate(Expression(["x[1]-0.5", "-x[0]+0.5"]))
#c = c0

cmesh.interpolate(Expression((0,0)))

c = c-cmesh

a = v*u*dx(degree=3) + 0.5*delta_t*(v*dot(c, grad(u))*dx(degree=3))
L = v*u0*dx(degree=3) - 0.5*delta_t*(v*dot(c, grad(u0))*dx(degree=3))
# Add SUPG stabilization terms
ra = u + 0.5*delta_t*(dot(c, grad(u)))
rL = u0 - 0.5*delta_t*(dot(c, grad(u0)))
cnorm = sqrt(dot(c, c))
h = Constant(1./n)
a += (h/(2.*cnorm))*dot(c, grad(v))*ra*dx(degree=3)
L += (h/(2.*cnorm))*dot(c, grad(v))*rL*dx(degree=3)

A_prob = LinearVariationalProblem(a,L,u0,bcs=bc, constant_jacobian=False)
pc = 'ilu'
A_solv = LinearVariationalSolver(A_prob, solver_parameters={'pc_type': pc,
                                                            'ksp_max_it' : 1500,
                                                            'ksp_converged_reason': True,
                                                            'ksp_monitor_true_residual': False,
                                                            'ksp_view': False})




######################## Moving mesh stuff 


mesh_crd0 = Function(W)

mesh_crd0.dat.data[:] = mesh.coordinates.dat.data

alpha = 0.2
T_mm = 1
t = 0

ndim = 2
entity_dofs = np.zeros(ndim+1, dtype=np.int32)
entity_dofs[0] = mesh.geometric_dimension()
coordSection = mesh._plex.createSection([1], entity_dofs, perm=mesh.topology._plex_renumbering)
    
plex = mesh._plex
vStart, vEnd = plex.getDepthStratum(0)
nbrVer = vEnd - vStart
    
dmCoords = mesh.topology._plex.getCoordinateDM()
dmCoords.setDefaultSection(coordSection)  




########################  Initial step

U_ini_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.75)*(x[1]-0.75)-0.15*0.15))", alpha = 5000)
#U_ini_exp = Expression("0.5-0.5*tanh(alpha*((x[0]-0.7)*(x[0]-0.7) + (x[1]-0.75)*(x[1]-0.75)-0.15*0.15))", alpha = 5000)
u0.interpolate(U_ini_exp)
    
#with mesh.coordinates.dat.vec_ro as coords:
#    mesh.topology._plex.setCoordinatesLocal(coords)
#writeGmf(mesh, 1, "boundary_ids", "bubblemm.%d" % 0, u0, 1, "bubblemm.%d" % 0, coordSection)

outfile = File("bubblemm.pvd")
outfile.write(u0) 

t = 0
ite = 1
dt = 0.1/n
while t <= 1: #0.5*T:
    
    print "##########  Ite: %d   t: %1.2e --> %1.2e" % (ite, t, t+dt)

    c0.interpolate(Expression([cxExpr, cyExpr]))
        
    ##### Move the mesh
    newCrd = mesh_crd0.dat.data[:,0] + alpha * np.sin(M_PI*mesh_crd0.dat.data[:,0]) * np.sin(2*M_PI*mesh_crd0.dat.data[:,1]) * sin(2*2*M_PI/T_mm *t)
    cmesh.dat.data[:,0] = (newCrd-mesh.coordinates.dat.data[:,0])/dt
    np.copyto(mesh.coordinates.dat.data[:,0], newCrd)

    cmeshp2.interpolate(cmesh)
    
#    cprint.interpolate(c0*cos(2*pi*time/T))
#    cprint.interpolate(c)
#    if (ite > 1): break

    ###### Solve the equation
    time.assign(t)
    delta_t.assign(dt)

    A_solv.solve()

    ucorr.dat.data[...] = np.maximum(u0.dat.data, 0)
    ucorr.dat.data[...] = np.minimum(ucorr.dat.data, 1)
    u0.assign(ucorr)

    ####### Update functions
#    u0 = Function(functionspaceimpl.WithGeometry(u0.function_space(), mesh), val=u0.topological)
#    c0 = Function(functionspaceimpl.WithGeometry(c0.function_space(), mesh), val=c0.topological)
#    cn = Function(functionspaceimpl.WithGeometry(cn.function_space(), mesh), val=cn.topological)

    ####### write to file
    iteSav = 20
    if (ite%iteSav == 0) :	
#        with mesh.coordinates.dat.vec_ro as coords:
#            mesh.topology._plex.setCoordinatesLocal(coords)
#        writeGmf(mesh, 1, "boundary_ids", "bubblemm.%d" % (ite/iteSav), u0, 1, "bubblemm.%d" %(ite/iteSav), coordSection)
        outfile.write(u0) 
	
    t += dt
    ite += 1
